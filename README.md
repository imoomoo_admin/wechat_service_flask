# 基于flask的企业微信服务端 #
## 安装步骤 ##
1. pip install flask
2. pip install vituralenv 
3. pip install -r requirements.txt

### 功能 ###
该应用程序基于flask构建，可满足企业微信自建应用的网页授权、扫码授权、身份验证、发送应用消息功能。
可以让从企业微信终端打开的网页获取成员的身份信息，从而免去登录的环节，无缝衔接各类已有应用。
程序接入流程：
1. 用户点击
2. 该企业微信服务器构造网页授权链接，根据redirect_uri 回调地址返回客户端
3. 客户端获取回调链接中的code值，并验证state值。
4. 客户端再次向该企业服务端发起请求，并携带code值（该code只能使用一次）
5. 该企业微信服务端 获取code值后，构造请求链接，向企业微信后台申请获取用户信息，并返回给客户端json格式数据
6. 客户端获取json格式数据后，自行进行后续处理

![http://p.qpic.cn/pic_wework/3033848529/181ef914a06abb1b1c775696a42f5cfcf7815f1675cdab77/0]企业微信OAuth2流程图

### 使用步骤 ###
1. 在config.py文件内，配置CORPID、AGENTS、AGENT_INDEX。
其中，
> AGENTS 保存企业自建应用的信息，数据格式为字典，key为企业微信自建应用的id，value为自建应用的secret。
> AGENT_INDEX 保存企业自建应用回调地址，key为企业微信自建应用的id，value为回调地址。

2. 配置tornado_server.py 的端口及ip地址。
> http_server.listen(9999, address="127.0.0.1")  #flask默认的端口为5000

3. 在企业微信后台管理，配置网页授权及JS-SDK
该配置可作为应用OAuth2.0网页授权功能的回调域名
注意：该可信域名，必须和AGENTS_INDEX 配置的回调地址相匹配。企业微信对回调域名地址有严格的规定，二级域名、端口号等
均需要一致才行。

4. 在企业微信后台，配置 工作台应用主页。
该配置用于企业用户点击自建应用后，向服务端发起OAuth 网页验证请求。
> 格式为：http://domain/wechat/oauth/agentid
> 其中：domain为第3步验证后的可信域名
> agentid为应用的id

5. 以下步骤必须自建应用程序客户端编程处理。
- 客户端获取code值，向该企业微信服务端 发生请求
> 请求地址格式：http://server-domain/wechat/getuser/code
> 其中：server-domain地址为第二步中配置的服务端地址和端口，
> code为第4步中获取的code值。

### 需要额外提醒的一点 ###
用户可架设ngnix，进行反向代理，该服务器软件端口和地址可不暴露。




