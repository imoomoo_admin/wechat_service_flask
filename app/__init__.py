import logging
from logging.handlers import RotatingFileHandler
import os
#从flask包中导入Flask类
from flask import Flask
#从config模块导入Config类
from config import Config

def create_app(config_class =  Config):
    #将Flask类的实例 赋值给名为 app 的变量。这个实例成为app包的成员。
    app = Flask(__name__)
    app.config.from_object(config_class)

    #身份验证蓝图
    from app.auth import bp as auth_bp
    # Flask提供了在URL前缀下附加蓝图的选项，任何在蓝图中定义的路由都会在它们的URL中取得前缀。
    # 现在登录URL将是 http://localhost:5000/auth/login
    app.register_blueprint(auth_bp)

    if not app.debug and not app.testing:
        if not os.path.exists('logs'):
            os.mkdir('logs')
        file_handler = RotatingFileHandler('logs/car_manager.log', maxBytes=10240, backupCount=10)
        file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)

        app.logger.setLevel(logging.INFO)
        app.logger.info('系统启动完成！')
    return app

