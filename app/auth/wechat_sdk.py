# -*- coding:utf-8 -*-
__author__ = "lovefox"

from flask import current_app
import json
import requests, urllib
import os.path
import pickle
import time
from config import Config


class ErrorCode(object):
    SUCCESS = 0

class WeChatEnterprise(object):
    def __init__(self, agentid = '1000006' ):
        """
            document address: http://qydev.weixin.qq.com/wiki/index.php?title=%E9%A6%96%E9%A1%B5
        """
        #self.corpid = CORPID
        self.corpid =current_app.config['CORPID']
        #self.corpsecret = AGENTS[agentid]
        self.agentid = agentid
        self.corpsecret = current_app.config['AGENTS'][self.agentid]
        #self.token_file = 'token.json'
        #self.token_name = 'access_token'
        self.url_prefix = "https://qyapi.weixin.qq.com/cgi-bin"
        #self.access_token = self.init_token_file(self.token_file)
    
    def __get_access_token(self):
        # access_token 有效期为 7200秒
        # todo 缓存access_token
        #url = "%s/gettoken?corpid=%s&corpsecret=%s" % (self.url_prefix, self.corpid, self.corpsecret)
        #res = requests.get(url)
        #access_token = res.json().get("access_token")
        #return access_token
        print('agentid:%s' % self.agentid)
        try:
            with open( self.agentid +'.json', 'r', encoding='utf-8') as f:
                token = json.load(f)
                if time.time() < token['expire']:
                    print(token['access_token'])
                    return token['access_token']
                else:
                    return self.__get_new_token()
        except FileNotFoundError:
            return self.__get_new_token()


    def __get_new_token(self):
        """
        功能：获取新token
        """
        url = 'https://qyapi.weixin.qq.com/cgi-bin/gettoken'
        params = {
            'corpid': self.corpid,
            'corpsecret': self.corpsecret
        }
        #print(self.corpid)
        #print(self.corpsecret)
        response = requests.get(url, params=params).text
        j = json.loads(response)
        if not j['errcode']:
            new_token = j['access_token']
            token_expire = j['expires_in']
            
            tokens = {}
            tokens['access_token'] = new_token
            tokens['expire'] = int(time.time()) + int(token_expire) - 200
            with open(self.agentid +'.json', 'w') as f:
                json.dump(tokens, f)
            return new_token
    
    def get_oauth_url(self, agentid):
        """
        功能：
        文档：https://work.weixin.qq.com/api/doc#90000/90135/91022
        网页授权链接:https://open.weixin.qq.com/connect/oauth2/authorize?appid=CORPID&redirect_uri=REDIRECT_URI&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect
            appid:企业的CorpID
            redirect_uri	是	授权后重定向的回调链接地址，请使用urlencode对链接进行处理
            response_type	是	返回类型，此时固定为：code
            scope	是	应用授权作用域。企业自建应用固定填写：snsapi_base
            state	否	重定向后会带上state参数，企业可以填写a-zA-Z0-9的参数值，长度不可超过128个字节
            #wechat_redirect	是	终端使用此参数判断是否需要带上身份信息
        """
        oauth_url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=CORPID&redirect_uri=REDIRECT_URI&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect"
        
        access_url = Config.AGENT_INDEX.get(agentid,'1000006')
        # 授权后重定向的回调链接地址，请使用urlencode对链接进行处理
        redirect_uri = {
            "redirect_uri": access_url
        }
        
        redirect_uri = urllib.parse.urlencode(redirect_uri)
        state = "hpi"
        oauth_url = oauth_url.replace("CORPID", Config.CORPID).replace("redirect_uri=REDIRECT_URI", redirect_uri).replace("STATE", state) 
        
        return oauth_url

    def get_user_detail(self, code):
        """
        功能：获取访问用户身份。该接口用于根据code获取成员信息
        请求方式：GET（HTTPS）
        请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token=ACCESS_TOKEN&code=CODE
        参数说明：
            参数	必须	说明
            access_token	是	调用接口凭证
            code	是	通过成员授权获取到的code，最大为512字节。每次成员授权带上的code将不一样，code只能使用一次，5分钟未被使用自动过期。
        权限说明：
            跳转的域名须完全匹配access_token对应应用的可信域名，否则会返回50001错误。
        返回结果：
            a) 当用户为企业成员时返回示例如下：
                {
                    "errcode": 0,
                    "errmsg": "ok",
                    "UserId":"USERID",
                    "DeviceId":"DEVICEID"
                }
            b) 非企业成员授权时返回示例如下：

                {
                    "errcode": 0,
                    "errmsg": "ok",
                    "OpenId":"OPENID",
                    "DeviceId":"DEVICEID"
                }

        """
        ACCESS_TOKEN = self.__get_access_token()
        url = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token=ACCESS_TOKEN&code=CODE"
        url = url.replace('ACCESS_TOKEN', ACCESS_TOKEN).replace('CODE', code)
        
        resp = requests.get(url)
        user_json = json.loads(resp.text)

        print(user_json)
        print('server errcode:', user_json['errcode'])

        if user_json['errcode'] == 0:
            """
                功能：根据userid 获取用户的详情。自建应用可以读取该应用设置的可见范围内的成员信息
                请求方式：GET（HTTPS）
                请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&userid=USERID
                参数说明：
                    参数	必须	说明
                    access_token	是	调用接口凭证
                    userid	是	成员UserID。对应管理端的帐号，企业内必须唯一。不区分大小写，长度为1~64个字节
                权限说明：
                    应用须拥有指定成员的查看权限。
            """
            get_userinfo_url = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&userid=USERID"
            get_userinfo_url = get_userinfo_url.replace('ACCESS_TOKEN', ACCESS_TOKEN).replace('USERID', user_json['UserId'])
            print(get_userinfo_url)
            resp = requests.get(get_userinfo_url)
            user_json = json.loads(resp.text)
            #print('wechat sdk:', user_json)
        
        return user_json

    def get_dept_userlist_from_deptid(self, deptid):
        """
            功能：根据部门id获取部门成员列表。可递归
            请求方式：GET（HTTPS）
            请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token=ACCESS_TOKEN&department_id=DEPARTMENT_ID&fetch_child=FETCH_CHILD
            参数说明：
                参数	必须	说明
                access_token	是	调用接口凭证
                department_id	是	获取的部门id
                fetch_child	否	1/0：是否递归获取子部门下面的成员
            权限说明：
                应用须拥有指定部门的查看权限。
        """
        ACCESS_TOKEN = self.__get_access_token()
        url = "https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token=ACCESS_TOKEN&department_id=DEPARTMENT_ID&fetch_child=FETCH_CHILD"
        url = url.replace('ACCESS_TOKEN', ACCESS_TOKEN).replace('DEPARTMENT_ID', deptid)
        return 'ok'

    def send_message(self, msgdata):
        """
        功能：发送消息
        请求方式：POST（HTTPS）
        请求地址： https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=ACCESS_TOKEN
        参数说明：
            参数	        是否必须	    说明
            access_token	是	        调用接口凭证
        """
        
        url = 'https://qyapi.weixin.qq.com/cgi-bin/message/send'
        params = {
            'access_token': self.__get_access_token()        
        }
       

        #print('msgdata1 type:', type(msgdata1))
        #print('dumps type:', type(json.dumps(msgdata1)))
        # post 数据时，如果用json 数据格式必须是字典， 如果用data 必须是 string 
        response = requests.post(url, params=params, json=msgdata)
        print('response :',response.text)
        return response.text

    def get_approval(self, appdata):
        """
        功能：获取审批数据
        请求方式：POST（HTTPS）
        请求地址：https://qyapi.weixin.qq.com/cgi-bin/corp/getapprovaldata?access_token=ACCESS_TOKEN
        
        参数说明：
            参数	必须	说明
            access_token	是	调用接口凭证。必须使用审批应用的Secret获取access_token
            starttime	是	获取审批记录的开始时间。Unix时间戳
            endtime	是	获取审批记录的结束时间。Unix时间戳
            next_spnum	否	第一个拉取的审批单号，不填从该时间段的第一个审批单拉取
        
        参数示例：
        {
            "starttime":  1492617600,
            "endtime":    1492790400,
            "next_spnum": 201704200003
        }

        进一步的详细说明请查阅：https://work.weixin.qq.com/api/doc#90000/90135/90265
        """
        url = 'https://qyapi.weixin.qq.com/cgi-bin/corp/getapprovaldata?access_token=ACCESS_TOKEN'
        ACCESS_TOKEN = self.__get_access_token()
        url = url.replace('ACCESS_TOKEN', ACCESS_TOKEN)
        response = requests.post(url, json = appdata)
        return response.text
        #return "get_approval"

class WeChatTocken(object):
    pass

"""
if __name__ == "__main__":
    wechat = WeChatEnterprise()
    wechat.agentid = '1000006'
    wechat.send_message('80039428', '测试')
"""
