import os

basedir = os.path.abspath(os.path.dirname(__file__))#获取当前.py文件的绝对路径

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you will never guess'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI') or 'sqlite:///' + os.path.join(basedir, 'app.db')
    #SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI') or 'mysql://root:123@localhost/car_manager' + 
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    ADMINS = '10000@QQ.COM'
    # 分页，每页的帖子数
    POSTS_PER_PAGE = 5
    # 程序的语言设置。en-US、en-GB、en-CA以支持美国、英国、加拿大，
    # zh-HK 香港，zh-MO 澳门，zh-TW 台湾，zh-SG 新加坡
    LANGUAGES = ['en', 'zh']

    # 所属企业的企业 ID
    CORPID = 'wxfa1b2b7e409f4d40'

    # 企业微信应用列表。用dict保存企业应用的信息。其中，Key对应AgentId，value对应Secret

    AGENTS = {
        "1000006": "bf8JV0FlDFOfUvTdkjcMN4BFsIDrqvQTXcM7s6q-Nac",
        "1000005": "NacDIR_ksM7nXJkxj2TyFeDsVtpdPN_FzOuA7iKaL3g-EED",
        "3010040": "jLAtlrXtrUv8QbDS3Y_wrqSjrlE4Y6N0_iKVBqloHKQ"
    }


    # 验证完成后，返回的自建应用的
    AGENT_INDEX = {
        "1000006": "http://domain:6666/auth/oauth"
    }

    # 下发消息的最大字符数，当消息大于 2048 字符时会被企业微信截断为前 2048 字符，长消息将会以该参数值为长度被切分为多条消息，默认为2000
    MESSAGELENGTH = 2000




